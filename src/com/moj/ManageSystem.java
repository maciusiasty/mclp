package com.moj;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by maciek on 12.04.17.
 */
public final class ManageSystem {


    public static ArrayList<Member> arrayOfMember = new ArrayList<Member>();


    public void addMember(int x,int y) {
        Member member = new Member(x,y);
        arrayOfMember.add(member);
    }

    public int getNumberOfMember() {
        return arrayOfMember.size();
    }


    public Member getMember(int i) {
        return arrayOfMember.get(i);
    }

    public ArrayList makeRandomList(int x) {
        List<Integer> uzyteX = new ArrayList<Integer>();
        List<Integer> uzyteY = new ArrayList<Integer>();
        Random random = new Random();
        for(int i=0;i<x*x;i++) {

            int liczbaX = random.nextInt(x-1);
            int liczbaY = random.nextInt(x-1);
            if(uzyteX.contains(liczbaX) && uzyteY.contains(liczbaY))
               continue;
            uzyteX.add(liczbaX);
            uzyteY.add(liczbaY);
            addMember(liczbaX,liczbaY);
        }


        return null;
    }

}
