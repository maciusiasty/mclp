package com.moj;

import java.util.ArrayList;

/**
 * Created by maciek on 12.04.17.
 */
public class Matrix {

    public int data[][];
    private int x,y;
    ArrayList<Integer> numberOfMember = new ArrayList<Integer>();
    public static Matrix instance = null;

    public static Matrix getInstance() {
        if(instance == null)
        {
            instance = new Matrix();
            return instance;
        }
        else return instance;
    }


    /*
    @Description Ustawia wymiary analizowanego obszaru
     */
    public void setDataDimension(int x,int y) {
        data = new int[x][y];
        this.x = x;
        this.y = y;
    }

    public Matrix() {}

    /*
    @Description Przeksztalca wlasciwosci klasy w tablice
     */
    public boolean trasnlateToMatrix(ManageSystem manageSystem) {

        for(int i=0;i<manageSystem.getNumberOfMember();i++) {
            if(Math.max(manageSystem.getMember(i).getX(),manageSystem.getMember(i).getY()) >= Math.max(x,y))
                return false;
            else
            data[manageSystem.getMember(i).getX()][manageSystem.getMember(i).getY()] = 1;
        }

        for(int i=0;i<x;i++) {

            for(int j=0;j<y;j++) {
                if(data[i][j] != 1 && data[i][j] !=2) {
                    data[i][j]=0;
                }
            }
        }

        return true;

    }

    /*
    @Description glowna funkcja wskazujaca polorzenie AccessPointow
     */

    public ArrayList<Integer> checkPlaceForAC(int d,ManageSystem manageSystem) {

        //pomocnicza tymczasowa macierz
        int dataTemp[][] = new int[x][y];
        //pomocnicza tymczasowe polorzenie x,y
        int tempx =0,tempy=0;

        //Aktualnie badana liczba klientów
        int numberOfMember = 0;
        //ostatnia zbadana liczba klientów
        int lastNumberOfMember = 0;
        //liczba klientów która zostaje wybrana (maxymalna)
        int wynik = 0;

        //pomocniczne tablice
        ArrayList<Integer> arrayOfWynik = new ArrayList<Integer>();
        ArrayList<Integer> tablicaX = new ArrayList<Integer>();
        ArrayList<Integer> tablicaY = new ArrayList<Integer>();


        // Usuwa możliwość wyjścia przesuwanego kwadratu poza rozważany obszar
        boolean flaga = false;

        int temp = 0;

        if(this.x < this.y) temp = this.x;
        else temp = this.y;
        //warunek który eleminuje sytuacje w której d jest za duże
        if(2*d >= temp) return null;

        int sum = 0;
        boolean czyZmiana = false;

        //klonowanie macierzy
        cloneMatrix(data,dataTemp);

        while(true) {

            //Petla która idzie po całym kwadracie
            for (int x = 0; x < this.x; x++) {
                for (int y = 0; y < this.y; y++) {
                    if (tablicaX.contains(x) && tablicaY.contains(y)) break;


                    //Petla idzie po małym kwadracie
                    for (int i = x; i < (2 * d + 1) + x; i++) {
                        if (flaga) break;
                        for (int j = y; j < (2 * d + 1) + y; j++) {

                            if ((i == this.x - 1 || j == this.y - 1) && !(2*d+y==j  || 2*d+x==i)) flaga = true;

                            if (dataTemp[i][j] == 1)
                                numberOfMember++;

                            if (flaga) {
                                numberOfMember = 0;
                                break;
                            }


                        }
                    }
                    flaga = false;

                    if (numberOfMember > lastNumberOfMember) {
                        czyZmiana = true;
                        tempx = x;
                        tempy = y;
                        wynik = numberOfMember;
                        lastNumberOfMember = numberOfMember;
                    }


                    numberOfMember = 0;


                }
            }

            sum+=wynik;
            tablicaX.add(tempx);
            tablicaY.add(tempy);
            arrayOfWynik.add(tempx);
            arrayOfWynik.add(tempy);
            arrayOfWynik.add(wynik);
            wynik = 0;


                if(czyZmiana)
            //zerowanie tablicy  w odpowiednich miejscach
            for (int i = tablicaX.get(tablicaX.size()-1); i < (2 * d + 1) + tablicaX.get(tablicaX.size()-1); i++)
                for(int j=tablicaY.get(tablicaY.size()-1);j<(2*d+1)+tablicaY.get(tablicaY.size()-1);j++) {

                    dataTemp[i][j] = 0;
                    czyZmiana = false;
                }
            tablicaX.clear();
            tablicaY.clear();
            lastNumberOfMember = 0;

            if(manageSystem.getNumberOfMember()<= sum) break;
        }



        return arrayOfWynik;

    }

    /*
    @Description Pokazuje macierz wizualnie
     */

    public void showVisualMatrix() {

        for(int i=0;i<this.x;i++) {
            for(int j=0;j<this.y;j++) {
                System.out.print(data[j][i]+" ");
            }
            System.out.println();
        }

    }

    /*
    @Description Kopiuje tablice do nowej tablicy
     */

    private void cloneMatrix(int myOld[][],int myNew[][]) {

        for(int i=0;i<myOld.length;i++)
            for (int j=0;j<myOld[i].length;j++) {
            myNew[i][j] = myOld[i][j];
            }

    }
}
