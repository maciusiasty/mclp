package com.moj;

/**
 * Created by maciek on 12.04.17.
 */
public class Member {

    private int x;
    private int y;

    public Member() {}

    public Member(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Member{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
