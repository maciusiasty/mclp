package com.genetics;


import java.util.*;

/**
 * Created by maciek on 29.04.17.
 */
public class Individual {

    private int fitness =0;
    //Zmiana na byte !!!
  public byte[] genes = new byte[Population.x];


    //Inicjalizuje wstępna populacje
    /*
    DO ZMIANY TODO
     */
    public void generateIndividual(int x) {

        genes = new byte[x];
        Random random = new Random();
      for(int i=0;i<genes.length;i++) {
          genes[i] = (byte) (random.nextInt(3));
      }
    }


    public byte getGene(int index) {
        return genes[index];
    }

    public void setGene(int index,byte value) {
        genes[index] = value;
    }



    public int getFitness() {

        if (fitness == 0) {
            fitness = FitnessCalc.getFitness(this);
        }
        return fitness;
    }

    public int size() {

        return genes.length;
    }

    @Override
    public String toString() {
        return "Individual{" +
                "fitness=" + fitness +
                ", genes=" + genes +
                '}';
    }

}
