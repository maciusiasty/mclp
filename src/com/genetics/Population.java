package com.genetics;

/**
 * Created by maciek on 29.04.17.
 */
public class Population {

    private Individual individuals[];
    public static int x;

    //Konstruktor dla Populacji
    public Population(int populationSize, boolean initialise) {
        individuals = new Individual[populationSize];
        //Inicjalizacja populacji
        if (initialise) {

            for (int i = 0; i < size(); i++) {
                Individual newIndividual = new Individual();
                newIndividual.generateIndividual(x);
                saveIndividual(i, newIndividual);
            }
        }
    }

    // Wybiera maxymalny fitness dla danej tablicy individuals
    public Individual getFittest() {

        Individual fittest = individuals[0];

        for(int i=0;i<size();i++) {
            if(fittest.getFitness() <= getIndividual(i).getFitness()) {
                fittest = getIndividual(i);
            }
        }
        return fittest;
    }


    //Zapisuje individual o danym indeksie
    public void saveIndividual(int i, Individual newIndividual) {

        individuals[i] = newIndividual;
    }

    public  void setDimension(int x) {
        this.x = x;
    }


    //Zwraca indvidual o danym indeksie
    public Individual getIndividual(int index) {
        return individuals[index];
    }

    //zwraca rozmiar tablicy individual
    public int size() {
        return individuals.length;
    }








}
