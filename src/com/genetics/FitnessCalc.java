package com.genetics;

import com.moj.ManageSystem;
import com.moj.Matrix;

import java.util.ArrayList;

/**
 * Created by maciek on 12.05.17.
 */
public class FitnessCalc {

    public static byte[] sollution = new byte[Population.x];

    static void setSollution(ManageSystem manageSystem, int x, int d) throws Exception {
        Matrix matrix = Matrix.getInstance();
        matrix.setDataDimension(x, x);

        boolean czyDalej = matrix.trasnlateToMatrix(manageSystem);


        if (czyDalej) {
            ArrayList<Integer> wynik = matrix.checkPlaceForAC(d, manageSystem);
            if (wynik != null) {
                for(int i=3;i<=wynik.size();i+=3) {
                    if(matrix.data[wynik.get(i-3)+d][wynik.get(i-2)+d]==1)
                        matrix.data[wynik.get(i-3)+d][wynik.get(i-2)+d] = 3;
                    else
                        matrix.data[wynik.get(i-3)+d][wynik.get(i-2)+d] = 2;
                }
                for(int i=0;i<matrix.data.length;i++) {
                    for(int j=0;j<matrix.data[i].length;j++) {
                        if(i<1) {
                            sollution[j] = (byte) matrix.data[i][j];
                        }
                        else {
                            String liczbaStr = String.valueOf(i)+String.valueOf(j);
                            int index = Integer.valueOf(liczbaStr);
                            sollution[index] = (byte) matrix.data[i][j];
                        }
                    }
                }




            } else throw new Exception("Za duży podany zasieg wzgledem rozwazanego obszaru.");
        }
        else throw new Exception("Polorzenie klientów poza rozwazanym obszarem.");
        }


    /*
 @Description Zwraca dopasowanie naszej populacji do wzorca
 */
    public static int getFitness(Individual individual) {
        int fitness = 0;

        for (int i = 0; i < individual.genes.length && i < sollution.length; i++) {
            if(sollution[i] == individual.getGene(i))
                fitness++;
        }

        return fitness;

    }

    public static int getMaxFitness() {

        return sollution.length;
    }

  /*
    @Description Pokazuje macierz wizualnie
     */

    public void showVisualMatrix() {

        for(int i=0;i<this.sollution.length;i++) {
            System.out.print(sollution[i]);
        }

    }


    }

