package com.genetics;

import com.moj.ManageSystem;
import com.moj.Matrix;

/**
 * Created by maciek on 28.04.17.
 */
public class MainClass {

    public static void main(String args[]) {

        //Nadanie wartości polorzenia klientom
        ManageSystem manageSystem = new ManageSystem();
        int d = 1;
        int x = 10;
        Population.x = x;
        manageSystem.makeRandomList(x);
        long startMoj = System.currentTimeMillis();

        //Generowanie randomowej populacji

       /*


        try {
            FitnessCalcSollution.setSollution("1111000000000000000000000000000000020003000200300000000000001111");
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        boolean err = false;

        try {
            FitnessCalc.setSollution(manageSystem, x, d);
        } catch (Exception ex) {
            if (ex.getMessage().contains("rozwazan"))
                err = true;
            System.out.println(ex.getMessage());
        }

        if (!err) {
            long stopMoj = System.currentTimeMillis();
            long iloscCzasuMoj = stopMoj - startMoj;

            long startAlorytm = System.currentTimeMillis();

            // Create an initial population

            Population myPop = new Population(50, true);

            // Evolve our population until we reach an optimum solution
            int generationCount = 0;
            while (myPop.getFittest().getFitness() < FitnessCalc.getMaxFitness()) {
                generationCount++;
                System.out.println("Generation: " + generationCount + " Fittest: " + myPop.getFittest().getFitness());
                myPop = Algorithm.evolvePopulation(myPop);
            }
            System.out.println("Solution found!");
            System.out.println("Generation: " + generationCount);

            long stopAlgorytm = System.currentTimeMillis();
            long wynikAlgorytm = stopAlgorytm - startAlorytm;

            Matrix matrix = Matrix.getInstance();
            matrix.showVisualMatrix();

            System.out.println("Algorytm genetyczny :" + wynikAlgorytm + " ms");
            System.out.println("Algorytm moj: " + iloscCzasuMoj + " ms");


        }
    }



    }

