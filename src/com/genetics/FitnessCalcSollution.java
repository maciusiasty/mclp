package com.genetics;

/**
 * Create=d by maciek on 29.04.17.
 */
public class FitnessCalcSollution {

    private static byte[] sollution = new byte[64];


    /*
    Jako arguement tablica byte
     */
    public static void setSollution(byte[] dane) {
        sollution = dane;
    }

    /*
    @Description Gdy jako argument jest String
     */
    public static void setSollution(String newSollution) throws Exception {
        String result = null;
        for (int i = 0; i < newSollution.length(); i++) {
            char character = newSollution.charAt(i);
            if (character == '1' || character == '0')
                sollution[i] = Byte.parseByte(String.valueOf(character));
            else throw new Exception("Sollution zawiera znaki spoza zakresu");

        }
    }

    /*
     @Description Zwraca dopasowanie naszej populacji do wzorca
     */
    public static int getFitness(Individual individual) {
        int fitness = 0;

        for (int i = 0; i < individual.genes.length && i < sollution.length; i++) {
            if(sollution[i] == individual.getGene(i))
                fitness++;
        }

        return fitness;

    }

    public static int getMaxFitness() {

        return sollution.length;
    }
}
